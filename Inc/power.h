/*
 *  @file power.h
 *
 *  @date Jan 14, 2020
 *  @author drid
 *  @brief Power status acquisition
 *
 *  @copyright Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef POWER_H_
#define POWER_H_

#include <max17261_driver.h>
#include "main.h"

#define ADC_VOLTAGE_COEFFICIENT		(1.395 * 3300 / 4096)
/**
 * @brief Power status structure
 *
 * Holds power related values
 */
struct power_status {
	uint16_t voltage; /**< Voltage in mV */
	uint16_t voltage_alt; /**< Voltage in mV sensed from STM32*/
	uint16_t voltage_avg; /**< Average Voltage in mV */
	uint16_t voltage_max; /**< Max Voltage in mV */
	uint16_t voltage_min; /**< Min Voltage in mV */
	int16_t current; /**< Current in mA */
	int16_t current_avg; /**< Average Current in mA */
	int16_t current_max; /**< Max in mA */
	int16_t current_min; /**< Min in mA */
	int8_t temperature_die; /**< Current in mA */
	int8_t temperature; /**< Current in mA */
	int8_t temperature_avg; /**< Average Current in mA */
	int8_t temperature_max; /**< Max in mA */
	int8_t temperature_min; /**< Min in mA */
	int8_t SOC; /**< State of charge in % */
	uint16_t TTE; /**< Time to empty in minutes */
	int16_t ant_deploy_test_current; /**< Relative current draw during antenna deployment test */
	int16_t ant_deploy_test_voltage; /**< Voltage during antenna deployment test */
};

void
update_power_status(struct power_status *power_status);

#endif /* POWER_H_ */
