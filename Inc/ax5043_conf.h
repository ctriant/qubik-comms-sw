

#ifdef USE_AX25
#define CALLSIGN_STATION                (uint8_t*) "XXXXXX/P"
#define CALLSIGN_DESTINATION            (uint8_t*) ""
#define TX_DATA_TYPE                    ax25_data_t

#endif

#ifdef USE_APRS
#define APRS_UHF                        432500000
#define TX_DATA_TYPE                    aprs_data_t
#endif

#ifndef TX_DATA_TYPE
#define TX_DATA_TYPE                    char[200]
#endif

#define RX_FREQ_HZ                      APRS_UHF
#ifdef USE_APRS
#define TX_FREQ_HZ                      APRS_UHF
#else
#define TX_FREQ_HZ                      432500000
#endif

/* Reference Oscillator frequency */
#if PQ9ISH_DEV_BOARD
#define XTAL_FREQ_HZ                    48000000
#else
#define XTAL_FREQ_HZ                    26000000
#endif
