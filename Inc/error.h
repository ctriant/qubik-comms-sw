

#ifndef ERROR_H_
#define ERROR_H_

/**
 * Qubik COMMS error codes
 */
enum {
	NO_ERROR,   //!< All ok
	INVAL_PARAM,//!< Invalid parameter
	OSDLP_QUEUE_EMPTY, //!< Access to empty queue
	OSDLP_QUEUE_MEMERROR, //!< Queue memory error
};

#endif /* ERROR_H_ */
