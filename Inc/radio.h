
#ifndef RADIO_H_
#define RADIO_H_

#include <stdlib.h>
#include <stdint.h>
#include <main.h>


#define MAX_RX_FRAME_LEN 1024
#define MAX_RX_FRAMES    16

struct rx_frame {
	uint32_t len;
	uint8_t pdu[MAX_RX_FRAME_LEN];
};

void
frame_received(const uint8_t *pdu, size_t len);

void
tx_complete();

#endif /* RADIO_H_ */
