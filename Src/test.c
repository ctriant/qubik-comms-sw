
#include <test.h>
#include <watchdog.h>
#include <conf.h>
#include <ax5043.h>
#include <cmsis_os.h>
#include <bsp_pq9ish_comms.h>
#include <max17261_driver.h>
#include "power.h"
#include "antenna.h"

extern struct ax5043_conf hax5043;
extern struct watchdog hwdg;
extern struct power_status power_status;
extern struct ant_status_t ant_status;

#if AX5043_DEBUG
#define TRACE_MAX_LEN 3200
static char regs_str[TRACE_MAX_LEN];
#endif

static void
trace_printf(const char *s, size_t slen)
{
	if (!s) {
		return;
	}
	for (size_t i = 0; i < slen; i++) {
		if (s[i] == 0) {
			break;
		}
		ITM_SendChar(s[i]);
	}
}

static void
test_rx(freq_mode_t fmode, uint32_t freq, uint32_t duration_ms)
{
	int ret;
	ret = ax5043_config_freq(&hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hax5043, fmode, TUNE_RX);
	if (ret) {
		Error_Handler();
	}

	struct rx_params r = {
		.mod = FSK,
		.baudrate = 9600,
		.bandwidth = 17400,
		.freq_offset_corr = SECOND_LO,
		.max_rf_offset = 2000,
		.dr_offset = 100,
		.en_diversity = false,
		.antesel = AX5043_RF_SWITCH_DISABLE,
		.fsk = {
			.mod_index = 1
		},
		.framing = RAW_PATTERN_MATCH,
		.raw_pattern = {
			.preamble = 0x5555,
			.preamble_len = 16,
			.preamble_max = 14,
			.preamble_unencoded = 1,
			.sync = 0x31e50000,
			.sync_len = 16,
			.sync_max = 14,
			.sync_unencoded = 1
		}
	};

	/* Test RX */
	ret = ax5043_conf_rx(&hax5043, &r);
	if (ret) {
		Error_Handler();
	}

	ret = ax5043_start_rx(&hax5043);
	if (ret) {
		Error_Handler();
	}

	for (size_t i = 0; i < duration_ms / 250; i++) {
		ret = ax5043_update_rx_status(&hax5043);
		if (ret) {
			Error_Handler();
		}
		osDelay(250);
	}

	ret = ax5043_stop_rx(&hax5043);
	if (ret) {
		Error_Handler();
	}
}


static void
test_cw(freq_mode_t fmode, uint32_t freq, uint32_t duration_ms)
{
	int ret;
	ret = ax5043_config_freq(&hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_conf_cw(&hax5043);
	if (ret) {
		Error_Handler();
	}

	/* Going for TX */
	ret = ax5043_set_pwramp(&hax5043, PA_ENABLE);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tx_cw(&hax5043, duration_ms);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_set_pwramp(&hax5043, PA_DISABLE);
	if (ret) {
		Error_Handler();
	}
}


static void
test_fsk_ax25(freq_mode_t fmode, uint32_t freq, uint32_t baud,
              uint32_t nframes, uint32_t delay_ms, uint8_t wdid)
{
	int ret;
	ret = ax5043_config_freq(&hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	struct tx_params p = {
		.mod = FSK,
		.baudrate = baud,
		.bandwidth = 2 * baud,
		.rf_out_mode = TXSE,
		.shaping = UNSHAPED,
		.pout_dBm = 16.0f,
		.fsk = {
			.mod_index = 1.0,
			.order = 1,
			.freq_shaping = GAUSIAN_BT_0_5
		},
		.framing = HDLC,
		.hdlc = {
			.en_nrz = 0,
			.en_nrzi = 1,
			.en_scrambler = 1,
			.preamble_len = 32,
			.postamble_len = 4
		}
	};
	ret = ax5043_conf_tx(&hax5043, &p);
	if (ret) {
		Error_Handler();
	}
	for (uint32_t i = 0; i < nframes; i++) {
		/* Read random memory and transmit it */
		ax5043_tx_frame(&hax5043, (const uint8_t *)&p, sizeof(p), 4000);
		watchdog_reset_subsystem(&hwdg, wdid);
		osDelay(delay_ms);
	}
}

static void
test_bpsk_ax25(freq_mode_t fmode, uint32_t freq, uint32_t baud,
               uint32_t nframes, uint32_t delay_ms, uint8_t wdid)
{
	int ret;
	ret = ax5043_config_freq(&hax5043, fmode, freq);
	if (ret) {
		Error_Handler();
	}
	ret = ax5043_tune(&hax5043, fmode, TUNE_TX);
	if (ret) {
		Error_Handler();
	}
	struct tx_params p = {
		.mod = PSK,
		.baudrate = baud,
		.bandwidth = 2 * baud,
		.rf_out_mode = TXSE,
		.shaping = RC,
		.pout_dBm = 16.0f,
		.psk = {
			.order = 1
		},
		.framing = HDLC,
		.hdlc = {
			.en_nrz = 0,
			.en_nrzi = 1,
			.en_scrambler = 1,
			.preamble_len = 64,
			.postamble_len = 4
		}
	};
	ret = ax5043_conf_tx(&hax5043, &p);
	if (ret) {
		Error_Handler();
	}
	for (uint32_t i = 0; i < nframes; i++) {
		/* Read random memory and transmit it */
		ax5043_tx_frame(&hax5043, (const uint8_t *)&p, sizeof(p), 4000);
		watchdog_reset_subsystem(&hwdg, wdid);
		osDelay(delay_ms);
	}
}

int
test_task()
{
	uint8_t wdgid = 0;
	/***************************************************************************
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * THIS TASK IS WATCHDOG ENABLED. TAKE CARE SO YOUR TESTS DO NOT EXECUTE
	 * LONGER THAN THE TIMEOUT OF THE IWDG
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	int ret = watchdog_register(&hwdg, &wdgid);
	if (ret) {
		return ret;
	}

	update_power_status(&power_status);
	ant_status.ant_deploy_test = antenna_deploy_test(&power_status);

#if TEST_ANT_DEPLOY
	ant_status.antenna_deploy_time = antenna_deploy();
#endif
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		update_power_status(&power_status);
		ant_status.ant_deploy_status = antenna_deploy_status();

#if TEST_RX
		test_rx(FREQA_MODE, 433200000, 2000);
#if AX5043_DEBUG
		ax5043_dump_regs(&hax5043);
		ax5043_dump_regs_str(regs_str, &hax5043, TRACE_MAX_LEN);
		trace_printf("RX=", 4);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_CW
		test_cw(FREQA_MODE, 433200000, 5000);
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_FSK_AX25
		test_fsk_ax25(FREQA_MODE, 433200000, 9600, 10, 1000, wdgid);
#if AX5043_DEBUG
		ax5043_dump_regs(&hax5043);
		ax5043_dump_regs_str(regs_str, &hax5043, TRACE_MAX_LEN);
		trace_printf("TX_FSK=", 8);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif

#if TEST_BSPK_AX25
		test_bpsk_ax25(FREQA_MODE, 433200000, 9600, 10, 1000, wdgid);
#if AX5043_DEBUG
		ax5043_dump_regs(&hax5043);
		ax5043_dump_regs_str(regs_str, &hax5043, TRACE_MAX_LEN);
		trace_printf("TX_BPSK=", 9);
		trace_printf(regs_str, TRACE_MAX_LEN);
#endif
		watchdog_reset_subsystem(&hwdg, wdgid);
#endif
		osDelay(1000);
	}
	return 0;
}
